package multithreading;

public class Atom implements Runnable {
    private int index = 0;
    private double p;
    private Crystal crystal;

    public Atom(Crystal crystal, double p) {
        this.crystal = crystal;
        this.p = p;
    }

    public void run() {
        while (true) {
            index = crystal.move(index, shouldMoveLeft());
        }
    }

    private boolean shouldMoveLeft() {
        return Math.random() <= p;
    }
}
