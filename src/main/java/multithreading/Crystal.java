package multithreading;

public interface Crystal {
    int move(int index, boolean moveLeft);
    void output();
    void stop();
    void start();
}
