package multithreading;

/**
 * Synchronized version of app
 */
public class Cells1 {

    /**
     * Number of cells
     */
    private static final int N = 10;

    /**
     * Number of atoms
     */
    private static final int K = 1000;
    private static final double p = 0.5;

    public static void main(String[] args) throws Exception {
        Crystal crystal = new CrystalSynchronizedImpl(N, K, p);
        crystal.output();
        crystal.start();
        for (int i = 0; i < 10; i++) {
            Thread.sleep(5000);
            crystal.output();
        }
        crystal.stop();
        System.out.println("Program finished. Number of atoms is consistent.");
    }
}
