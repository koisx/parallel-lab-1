package multithreading;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class CrystalIncorrectlySynchronizedImpl implements Crystal {
    private final int[] cells;
    private List<Thread> atoms = new LinkedList<>();
    private Object[] locks;

    public CrystalIncorrectlySynchronizedImpl(int N, int K, double p) {
        cells = new int[N];
        cells[0] = K;

        locks = new Object[N - 1];
        for (int i = 0; i < locks.length; i++) {
            locks[i] = new Object();
        }
        for (int i = 0; i < K; i++) {
            atoms.add(new Thread(new Atom(this, p)));
        }
    }

    public void start() {
        atoms.forEach(Thread::start);

    }

    public void stop() {
        atoms.forEach(Thread::interrupt);
    }

    public void output() {
        synchronized (cells) {
            int[] temp = cells.clone();
            int sum = 0;
            for (int value : temp) {
                System.out.print("[" + value + "] ");
                sum += value;
            }
            System.out.print("   ");
            System.out.println("sum:" + sum);
        }
    }

    @Override
    public int move(int index, boolean moveLeft) {
        if (moveLeft) {
            if (index == 0) return index;
            else {
                synchronized (locks[index - 1]) {
                    cells[index]--;
                    cells[index - 1]++;
                    return index - 1;
                }
            }
        } else {
            if (index == cells.length - 1) return index;
            else {
                synchronized (locks[index]) {
                    cells[index]--;
                    cells[index + 1]++;
                    return index + 1;
                }
            }
        }
    }
}
