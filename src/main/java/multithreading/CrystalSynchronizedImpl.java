package multithreading;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class CrystalSynchronizedImpl implements Crystal {
    private final int[] cells;
    private List<Thread> atoms = new LinkedList<>();

    public CrystalSynchronizedImpl(int N, int K, double p) {
        cells = new int[N];
        cells[0] = K;
        for (int i = 0; i < K; i++) {
            atoms.add(new Thread(new Atom(this, p)));
        }
    }

    public void start() {
        atoms.forEach(Thread::start);
    }

    public void stop() {
        atoms.forEach(Thread::interrupt);
    }

    public void output() {
        synchronized (cells) {
            int[] temp = cells.clone();
            int sum = 0;
            for (int value : temp) {
                System.out.print("[" + value + "] ");
                sum += value;
            }
            System.out.print("   ");
            System.out.println("sum:" + sum);
        }
    }

    @Override
    public int move(int index, boolean moveLeft) {
        synchronized (cells) {
            if (moveLeft) {
                if (index == 0) return index;
                else {
                    cells[index]--;
                    cells[index - 1]++;
                    return index - 1;
                }
            } else {
                if (index == cells.length - 1) return index;
                else {
                    cells[index]--;
                    cells[index + 1]++;
                    return index + 1;
                }
            }
        }
    }
}
